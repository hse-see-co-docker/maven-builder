FROM .
MAINTAINER abaschen

COPY *.crt $MAVEN_HOME/

ENV KEYSTORE_SECRET=changeit

RUN keytool -import -noprompt -trustcacerts -alias "CERN Root Certification Authority 2" -file $MAVEN_HOME/CERNRootCertificationAuthority2.crt -keystore $JAVA_HOME/jre/lib/security/cacerts --storepass $KEYSTORE_SECRET && \
    keytool -import -noprompt -trustcacerts -alias "CERN Certification Authority(1)" -file $MAVEN_HOME/CERNCertificationAuthority1.crt -keystore $JAVA_HOME/jre/lib/security/cacerts --storepass $KEYSTORE_SECRET && \
    keytool -import -noprompt -trustcacerts -alias "CERN Certification Authority" -file $MAVEN_HOME/CERNCertificationAuthority.crt -keystore $JAVA_HOME/jre/lib/security/cacerts --storepass $KEYSTORE_SECRET && \
    keytool -import -noprompt -trustcacerts -alias "CERN Grid Certification Authority" -file $MAVEN_HOME/CERNGridCertificationAuthority.crt -keystore $JAVA_HOME/jre/lib/security/cacerts --storepass $KEYSTORE_SECRET

RUN echo javax.xml.accessExternalSchema = all>>$JAVA_HOME/jre/lib/jaxp.properties